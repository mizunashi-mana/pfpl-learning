# PFPL Learning Note

## Artifacts

* [PDF on GitLab](https://gitlab.com/mizunashi-mana/pfpl-learning/-/jobs/artifacts/master/file/main.pdf?job=release-pdf)

## Installation

```bash
make
```

or using `docker-compose`

### Dependencies

* Pipenv
* Tlmgr
* pLaTeX
* LatexMk
* findutils (for clean task)
* git supports (optional: See https://git-scm.com/)
* editorconfig supports (optional: See http://editorconfig.org)
