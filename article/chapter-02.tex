\section{Inductive Definitions}

\begin{definition}[判定と判定形式]
  abt に対しての $n$-項関係を判定 (judgement) と呼ぶ．$n$-項の判定 $\mathrm{J}$ 及び abt $a_1, \ldots, a_n$ について $\mathop{\mathrm{J}} a_1 \cdots a_n$ を $\mathrm{J}$ に対する判定形式 (judgement form) と呼ぶ．
\end{definition}

\begin{definition}[推論規則]
  判定 $\mathrm{J}$ に対する判定形式 $J_1, \ldots, J_k, J$ について，その組
  \begin{align*}
    \infer{J}{J_1 & \cdots & J_k}
  \end{align*}
  を $\mathrm{J}$ の推論規則 (inference rule) と呼ぶ．
\end{definition}

\begin{definition}[帰納的定義]
  判定 $\mathrm{J}$ に対する推論規則の集合を帰納的定義 (inductive definition) と呼ぶ．
\end{definition}

なお，帰納的定義は多くの場合無限集合になるが，その表現方法として\emph{規則スキーム\emphp{rule scheme}}の概念を導入する．規則スキームは，abt の部分木を一部メタ変数にした推論規則の表記方法で，そのメタ変数に実際の abt を割り当てた推論規則全てを表す．

\newcommand*{\jdnat}{\mathop{\mathrm{nat}}}
\newcommand*{\szero}{\mathrm{zero}}
\newcommand*{\ssucc}{\mathrm{succ}}

\begin{example}[$- \jdnat$ の帰納的定義]
  \examlabel{judgement-nat}
  $- \jdnat$ の帰納的定義は，以下の規則スキームによって構成される．
  \begin{gather*}
    \infer[(\text{I-Z})]{\szero \jdnat}{}
    \hspace{2em}
    \infer[(\text{I-S})]{\ssucc(a) \jdnat}{a \jdnat}
  \end{gather*}
\end{example}

\newcommand*{\jdtree}{\mathop{\mathrm{tree}}}
\newcommand*{\sempty}{\mathrm{empty}}
\newcommand*{\snode}{\mathrm{node}}

\begin{example}[$- \jdtree$ の帰納的定義]
  \examlabel{judgement-tree}
  $- \jdtree$ の帰納的定義は，以下の規則スキームによって構成される．
  \begin{gather*}
    \infer[(\text{I-E})]{\sempty \jdtree}{}
    \hspace{2em}
    \infer[(\text{I-N})]{\snode(a_1; a_2) \jdtree}{a_1 \jdtree & a_2 \jdtree}
  \end{gather*}
\end{example}

\newcommand*{\jdis}{\mathrel{\mathrm{is}}}

\begin{example}[$- \jdis -$ の帰納的定義]
  \examlabel{judgement-is}
  $- \jdis$ の帰納的定義は，以下の規則スキームによって構成される．
  \begin{gather*}
    \infer[(\text{I-Z})]{\szero \jdis \szero}{}
    \hspace{2em}
    \infer[(\text{I-S})]{\ssucc(a_1) \jdis \ssucc(a_2)}{a_1 \jdis a_2}
  \end{gather*}
\end{example}

\begin{definition}[導出]
  判定 $\mathrm{J}$ の帰納的定義 $D$ に対する判定形式 $J$ の導出とは，$J_1, \ldots, J_k$ の導出 $\triangledown_1, \ldots, \triangledown_k$ と帰納的定義に含まれる推論規則
  \begin{align*}
    \left(\vcenter{\infer{J}{J_1 & \cdots & J_k}}\right) \in D
  \end{align*}
  に対して，
  \begin{align*}
    \infer{J}{\triangledown_1 & \cdots & \triangledown_k}
  \end{align*}
  のこと．$J$ に関する導出が存在する時，$J$ は導出できるという．この時，$J$ が成り立つという場合がある．
\end{definition}

\begin{lemma}[反転補題 (inversion lemma)]
  判定 $\mathrm{J}$ の帰納的定義 $D$，判定形式 $J$ に対して，$J$ が導出可能な時，以下が成り立つ．
  \begin{align*}
    \bigvee_{(J_1  \cdots J_k \mid J) \in D} J_1 \land \cdots \land J_k
  \end{align*}
\end{lemma}

\begin{principle}[規則帰納法の原理]
  \prinlabel{rule-induction}
  判定 $\mathrm{J}$ の帰納的定義 $D$，述語 $P$ について，
  \begin{align*}
    \forall \left(\vcenter{\infer{J}{J_1 & \cdots & J_k}}\right) \in D\ldotp P(J_1) \land \cdots \land P(J_k) \implies P(J)
  \end{align*}
  が成り立つ時，導出可能な $J$ に対して，$P(J)$ が成り立つ．
\end{principle}

なお，判定形式 $J = \mathop{\mathrm{J}} a_1 \cdots a_n$ について，$P(J)$ と書く代わりに $P(a_1, \ldots, a_n)$ と書く．\prinref{rule-induction}により示すことを，判定 $\mathrm{J}$ に関する\emph{規則帰納法\emphp{rule induction}}で示すという．

\begin{theorem}
  判定 $\mathrm{J}$ の帰納的定義 $D$，述語 $P$ について，$P'(J) \iff J \land P(J)$ とおく．
  \begin{align*}
    \forall \left(\vcenter{\infer{J}{J_1 & \cdots & J_k}}\right) \in D\ldotp P'(J_1) \land \cdots \land P'(J_k) \land J \implies P(J)
  \end{align*}
  が成り立つ時，導出可能な $J$ に対して，$P(J)$ が成り立つ．
\end{theorem}
\begin{proof}
  $P'$ を，$\mathrm{J}$ に関する規則帰納法で示す．
  \begin{align*}
    \left(\vcenter{\infer{J}{J_1 & \cdots & J_k}}\right) \in D
  \end{align*}
  について，$P'(J_1) \land \cdots \land P'(J_k)$ の時，$J_1, \ldots, J_k$ に対する導出が存在し，よって $J$ に対する導出が存在する．ここから仮定より，$P(J)$ が成り立つため，$P'(J)$ が成り立つ．ここから，導出可能な $J$ に対して $P'(J)$ が成り立つことから $P(J)$ が成り立つ．
\end{proof}

よって，帰納法の仮定において，推論規則の帰結が導出可能であることを，特に断りなく用いる．

\begin{lemma}
  \lemlabel{succ-nat-inversion}
  $\ssucc(a) \jdnat \implies a \jdnat$
\end{lemma}
\begin{proof}
  \examref{judgement-nat}において，$\ssucc(a)$ が結論にくる推論規則は，I-S だけである．よって，反転補題より明らか．
\end{proof}

\begin{lemma}
  $a \jdnat \implies a \jdis a$
\end{lemma}
\begin{proof}
  \begin{align*}
    P(a) \iff a \jdis a
  \end{align*}
  とおき，$P$ を\examref{judgement-nat}における規則帰納法で示す．
  \begin{description}
    \item[I-Z] \examref{judgement-is}の I-Z により $P(\szero)$ は明らか．
    \item[I-S] $P(a)$ の時，$a \jdis a$ に対する導出
    \begin{align*}
      \infer{a \jdis a}{\vdots}
    \end{align*}
    が存在し，この時，
    \begin{align*}
      \infer[(\text{I-S})]{\ssucc(a) \jdis \ssucc(a)}{\infer{a \jdis a}{\vdots}}
    \end{align*}
    より $P(\ssucc(a))$ となるため，正しい．
  \end{description}
\end{proof}

\begin{lemma}
  $\ssucc(a_1) \jdis \ssucc(a_2) \implies a_1 \jdis a_2$
\end{lemma}
\begin{proof}
  \lemref{succ-nat-inversion}と同様，反転補題より明らか．
\end{proof}

帰納的定義は，反復的，同時的な拡張が考えられる．これは，推論規則に複数の判定を含めるようにすることで，行われる．以降は，そのような拡張を施したもので考える．

\newcommand*{\jdsum}{\mathop{\mathrm{sum}}}

\begin{example}[$\jdsum(-; -; -)$ の帰納的定義]
  \examlabel{judgement-sum}
  $\jdsum(-; -; -)$ の帰納的定義は，以下の規則スキームによって構成される．
  \begin{gather*}
    \infer[(\text{I-Z})]{\jdsum(\szero; a; a)}{a \jdnat}
    \hspace{2em}
    \infer[(\text{I-S})]{\jdsum(\ssucc(a_1); a_2; \ssucc(a_3))}{\jdsum(a_1; a_2; a_3)}
  \end{gather*}
\end{example}

\begin{proposition}
  $a_1 \jdnat \land a_2 \jdnat \implies \exists! b\ldotp b \jdnat \land \jdsum(a_1; a_2; b)$
\end{proposition}
\begin{proof}
  \renewcommand*{\tenumlabel}[1]{\enumlabel{judgement-sum}{#1}}
  \renewcommand*{\tenumref}[1]{\enumref{judgement-sum}{#1}}

  以下の2つを示すことで，示す．
  \begin{enumerate}
    \item\tenumlabel{existence} $a_1 \jdnat \land a_2 \jdnat \implies \exists b\ldotp b \jdnat \land \jdsum(a_1; a_2; b)$
    \item\tenumlabel{uniqueness} $\jdsum(a_1; a_2; b_1) \land \jdsum(a_1; a_2; b_2) \implies b_1 \jdis b_2$
  \end{enumerate}

  \tenumref{existence}は，
  \begin{align*}
    P(a_1) \textiff \forall a_2\ldotp a_2 \jdnat \implies \exists b\ldotp b \jdnat \land \jdsum(a_1; a_2; b)
  \end{align*}
  を\examref{judgement-nat}における規則帰納法で示すことで，示す．
  \begin{description}
    \item[I-Z] $b = a_2$ とおくと，\examref{judgement-nat}の I-Z より明らかに条件を満たす．よって，$P(\szero)$ より正しい．
    \item[I-S] $P(a_1)$ が成り立つ時，$a_2 \jdnat$ を満たす $a_2$ について，$b' \jdnat \land \jdsum(a_1; a_2; b')$ を満たす $b'$ が存在する．この時 $b = \ssucc(b')$ とおくと，\examref{judgement-nat}の I-S より $b \jdnat$ で，\examref{judgement-sum} の I-S より $\jdsum(\ssucc(a_1); a_2; b)$ である．よって，この $b$ は条件を満たし，$P(\ssucc(a_1))$ より正しい．
  \end{description}

  \tenumref{uniqueness}は，
  \begin{align*}
    P_1(a_1, a_2, b_1) \textiff \forall b_2\ldotp \jdsum(a_1; a_2; b_2) \implies b_1 \jdis b_2
  \end{align*}
  を\examref{judgement-sum}における規則帰納法で示すことで，示す．
  \begin{description}
    \item[I-Z] この時，$a_1 = \szero$，$a_2 = b_1$，$b_1 \jdnat$ であり，任意の $a'_1$，$a'_2$，$b_2$ について，
    \begin{align*}
      P_2(a'_1, a'_2, b_2) \textiff a'_1 = \szero \land a'_2 = a_2 \implies b_1 \jdis b_2
    \end{align*}
    が成り立てば示せる．これを\examref{judgement-sum}における規則帰納法で示す．
    \begin{description}
      \item[I-Z] この時 $a'_1 = \szero$，$a'_2 = b_2$ が成り立つ．$a'_2 = a_2$ ならば $b_1 = a_2 = a'_2 = b_2$ より\lemref{succ-nat-inversion}から $b_1 \jdis b_2$．よって，正しい．
      \item[I-S] この時 $a'_1 = \ssucc(\ldots) \neq \szero$ より，vacuous に明らか．
    \end{description}
    \item[I-S] $P_1(a_1, a_2, b_1)$ の時，任意の $a'_1$，$a'_2$，$b_2$ について，
    \begin{align*}
      P_2(a'_1, a'_2, b_2) \textiff a'_1 = \ssucc(a_1) \land a'_2 = a_2 \implies \ssucc(b_1) \jdis b_2
    \end{align*}
    が成り立てば示せる．これを\examref{judgement-sum}における規則帰納法で示す．
    \begin{description}
      \item[I-Z] この時 $a'_1 = \szero \neq \ssucc(a_1)$ より，vacuous に明らか．
      \item[I-S] $P_2(a'_1, a'_2, b_2)$，$\jdsum(a'_1; a'_2; b_2)$ の時，$\ssucc(a'_1) = \ssucc(a_1)$，$a'_2 = a_2$ とすると，$a'_1 = a_1$ であり，$P_1(a_1, a_2, b_1)$，$\jdsum(a_1; a_2; b_2)$ から $b_1 \jdis b_2$．よって，\examref{judgement-is}の I-S から $\ssucc(b_1) \jdis \ssucc(b_2)$ より，$P_2(\ssucc(a'_1), a'_2, \ssucc(b_2))$．よって，正しい．
    \end{description}
  \end{description}
\end{proof}

\newcommand*{\jdmax}{\mathop{\mathrm{max}}}

\begin{example}[$\jdmax(-; -; -)$ の帰納的定義]
  \examlabel{judgement-max}
  $\jdmax(-; -; -)$ の帰納的定義は，以下の規則スキームによって構成される．
  \begin{gather*}
    \infer[(\text{I-LZ})]{\jdmax(\szero; a; a)}{a \jdnat}
    \hspace{2em}
    \infer[(\text{I-RZ})]{\jdmax(a; \szero; a)}{a \jdnat}
    \hspace{2em}
    \infer[(\text{I-S})]{\jdmax(\ssucc(a_1); \ssucc(a_2); \ssucc(a_3))}{\jdmax(a_1; a_2; a_3)}
  \end{gather*}
\end{example}

\begin{proposition}
  \proplabel{judgement-max-is-function}
  $a_1 \jdnat \land a_2 \jdnat \implies \exists! b\ldotp b \jdnat \land \jdmax(a_1; a_2; b)$
\end{proposition}
\begin{proof}
  \renewcommand*{\tenumlabel}[1]{\enumlabel{judgement-max}{#1}}
  \renewcommand*{\tenumref}[1]{\enumref{judgement-max}{#1}}

  以下の2つを示すことで，示す．
  \begin{enumerate}
    \item\tenumlabel{existence} $a_1 \jdnat \land a_2 \jdnat \implies \exists b\ldotp b \jdnat \land \jdmax(a_1; a_2; b)$
    \item\tenumlabel{uniqueness} $\jdsum(a_1; a_2; b_1) \land \jdmax(a_1; a_2; b_2) \implies b_1 \jdis b_2$
  \end{enumerate}

  \tenumref{existence}は，
  \begin{align*}
    P(a_1) \textiff \forall a_2\ldotp a_2 \jdnat \implies \exists b\ldotp b \jdnat \land \jdmax(a_1; a_2; b)
  \end{align*}
  を\examref{judgement-nat}における規則帰納法で示すことで，示す．
  \begin{description}
    \item[I-Z] $b = a_2$ とおくと，\examref{judgement-max}の I-LZ より明らかに条件を満たす．よって，$P(\szero)$ より正しい．
    \item[I-S] $P(a_1)$ が成り立つ時，$a_2$ の形で場合分けを行う．
    \begin{description}
      \item[$a_2 = \szero$ の時] $b = \ssucc(a_1)$ とおくと，\examref{judgement-max}の I-RZ より明らかに条件を満たす．よって，正しい．
      \item[$a_2 = \ssucc(a_2')$ の時] inversion lemma から，$\jdnat a_2'$ より，i.h. から $b' \jdnat \land \jdmax(a_1; a_2'; b')$ を満たす $b'$ が存在する．この時、$b = \ssucc(b')$ とおくと，\examref{judgement-max}の I-S より条件を満たす．よって，正しい．
    \end{description}
  \end{description}

  \tenumref{uniqueness}は，
  \begin{align*}
    P(a_1, a_2, b_1) \textiff \forall b_2\ldotp \jdmax(a_1; a_2; b_2) \implies b_1 \jdis b_2
  \end{align*}
  を\examref{judgement-max}における規則帰納法で示すことで，示す．
  \begin{description}
    \item[I-LZ] この時，$a_1 = \szero$，$a_2 = b_1$，$b_1 \jdnat$ である．この時，$\jdmax(a_1; a_2; b_2)$ となる $b_2$ について，$a_2$ の場合分けにより，inversion lemma から示す．
    \begin{description}
      \item[$a_2 = \szero$ の時] この時，$a_2 = b_2$ または $a_1 = b_2$ となるが，どちらの場合も $b_2 = \szero = a_2 = b_1$ である．よって，正しい．
      \item[$a_2 = \ssucc(a_2')$ の時] inversion lemma から適用される規則は一意であり，直ちに示せる．
    \end{description}
    \item[I-RZ] I-LZ と同様に示せる．
    \item[I-S] $P(a_1, a_2, b_1)$ の時，$\jdmax(\ssucc(a_1); \ssucc(a_2); b_2)$ の導出において，inversion lemma より最後に適用される規則は一意であり，直ちに示せる．
  \end{description}
\end{proof}

\newcommand*{\jdheight}{\mathop{\mathrm{hgt}}}

\begin{example}[$\jdheight(-; -; -)$ の帰納的定義]
  \examlabel{judgement-height}
  $\jdheight(-; -)$ の帰納的定義は，以下の規則スキームによって構成される．
  \begin{gather*}
    \infer[(\text{I-E})]{\jdheight(\sempty; \szero)}{}
    \hspace{2em}
    \infer[(\text{I-N})]{\jdheight(\snode(t_1; t_2); \ssucc(n))}{\jdheight(t_1; n_1) & \jdheight(t_2; n_2) & \jdmax(n_1; n_2; n)}
  \end{gather*}
\end{example}

\begin{proposition}
  $t \jdtree \implies \exists! n\ldotp n \jdnat \land \jdheight(t; n)$
\end{proposition}
\begin{proof}
  \renewcommand*{\tenumlabel}[1]{\enumlabel{judgement-height}{#1}}
  \renewcommand*{\tenumref}[1]{\enumref{judgement-height}{#1}}

  以下の2つを示すことで，示す．
  \begin{enumerate}
    \item\tenumlabel{existence} $t \jdtree \implies \exists n\ldotp n \jdnat \land \jdheight(t; n)$
    \item\tenumlabel{uniqueness} $\jdheight(t; n_1) \land \jdmax(t; n_2) \implies n_1 \jdis n_2$
  \end{enumerate}

  \tenumref{existence}は，
  \begin{align*}
    P(t) \textiff \forall \exists n\ldotp n \jdnat \land \jdheight(t; n)
  \end{align*}
  を\examref{judgement-tree}における規則帰納法で示すことで，示す．
  \begin{description}
    \item[I-E] $n = \szero$ とおくと，\examref{judgement-height}の I-E より明らかに条件を満たす．よって，$P(\sempty)$ より正しい．
    \item[I-S] $P(t_1)$，$P(t_2)$ が成り立つ時，$\jdheight(t_1; n_1)$，$\jdheight(t_2; n_2)$ を満たす $n_1 \jdnat$，$n_2 \jdnat$ が存在し，\propref{judgement-max-is-function}より $\jdmax(n_1; n_2; n')$ を満たす $n' \jdnat$ が存在する．よって，\examref{judgement-height}の I-N より，$n = \ssucc(n')$ とすると条件を満たす．よって，$P(\snode(t_1; t_2))$ より正しい．
  \end{description}

  \tenumref{uniqueness}は，\propref{judgement-max-is-function}及び定義から，帰納的定義が決定的であるため，inversion lemma より直ちに示せる．
\end{proof}

TODO: 他の演習
