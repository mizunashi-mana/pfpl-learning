\section{Abstract Syntax}

\subsection{Abstract Syntax Trees}

\begin{description}
  \item[種 (sort)] 値の種類のこと．要は，\emph{基本型\emphp{base type}}．通常，種の数は有限．
\end{description}

\begin{definition}[アリティ (arity)]
  種の有限集合 $\Sorts$ について，$\langle (s_1, \ldots, s_n), s\rangle \in \Sorts^* \times \Sorts$ \footnote{本文では，$(s_1, \ldots, s_n)s$ と表記している．}を，アリティと呼ぶ．アリティの集合を $\Arity(\Sorts) = \Sorts^* \times \Sorts$ と表記する．
\end{definition}

\begin{definition}[演算子 (operator)]
  種の有限集合 $\Sorts$ について，集合 $\Operators$ と関数 $\arity: \Operators \to \Arity(\Sorts)$ の組 $\langle \Sorts, \arity\rangle$ \footnote{本文では，アリティで添字づけられた互いに素な族として定義している．}を $\Sorts$-演算子の集合と呼ぶ．$o \in \Operators$ をアリティ $\arity(o)$ を持つ演算子と呼ぶ．また，文脈から $\arity$ が明らかな場合，単に $\Operators$ を演算子の集合と呼ぶ．アリティ $\langle (s_1, \ldots, s_n), s\rangle \in \Arity(\Sorts)$ について，
  \begin{align*}
    (s_1, \ldots, s_n) \sortto_\Operators s = \{o \in \Operators \mid \arity(o) = \langle (s_1, \ldots, s_n), s\rangle\}
  \end{align*}
  と表記する．なお， $\Operators$ が文脈から明らかな場合，$(s_1, \ldots, s_n) \sortto s = (s_1, \ldots, s_n) \sortto_\Operators s$ のように $\Operators$ を省略する場合がある．
\end{definition}

\begin{definition}[種付き変数 (variable with sort)]
  種の有限集合 $\Sorts$ について，互いに素な $\Sorts$ で添字づけられた族 $\Variables = \indexedfamily{s \in \Sorts}{\Variables_s}$ を，$\Sorts$-種付き変数の族と呼ぶ．また，$s \in \Sorts$ について，$x \in \Variables_s$ を $s$ 種を持つ変数 $x$ と呼ぶ．
\end{definition}

\begin{definition}[フレッシュな変数 (fresh variable)]
  種付き変数の族 $\Variables$ について，以下を満たす $x$ をフレッシュであると言う．
  \begin{align*}
    \forall s \in \Sorts\ldotp x \not\in \Variables_s
  \end{align*}
  この時，$s \in \Sorts$ に対して，$\Variables, x \mapsto s$ を，以下を満たす種付き変数の族 $\indexedfamily{s' \in \Sorts}{\Variables'_{s'}}$ で定義する．
  \begin{align*}
    \forall s' \in \Sorts\ldotp \Variables'_{s'} = \left\{\begin{array}{ll}
      \Variables_s \cup \{x\} &(s' = s) \\
      \Variables_{s'} &(\otherwise)
    \end{array}\right.
  \end{align*}
  なお，$s$ が文脈から明らかな場合，$\Variables, x = \Variables, x \mapsto s$ のように $s$ を省略する場合がある．
\end{definition}

\begin{definition}[抽象構文木 (abstract syntax tree, ast)]
  種の有限集合 $\Sorts$，$\Sorts$-演算子の集合 $\Operators$，$\Sorts$-種付き変数の族 $\Variables$ について，$\Ast_\Operators[\Variables] = \indexedfamily{s \in \Sorts}{\Ast_\Operators[\Variables]_s}$ を以下のように帰納的に定義する．
  \begin{description}
    \item[IB1] $s \in \Sorts$，$x \in \Variables_s$ について，$x \in \Ast_\Operators[\Variables]_s$．
    \item[IB2] $s \in \Sorts$，$o: () \sortto_\Operators s$ について，$o \in \Ast_\Operators[\Variables]_s$．
    \item[IS] $\langle(s_1, \ldots, s_n), s\rangle \in \Arity(\Sorts)$，$n \geq 1$，$o: (s_1, \ldots, s_n) \sortto_\Operators s$，$t_1 \in \Ast_\Operators[\Variables]_{s_1}, \ldots, t_n \in \Ast_\Operators[\Variables]_{s_n}$ について，$o(t_1; \cdots; t_n) \in \Ast_\Operators[\Variables]_s$．
  \end{description}
  この時，$t \in \Ast_\Operators[\Variables]$ を $\Variables$ で添字づけられた $\Operators$ による抽象構文木と呼ぶ．なお，$\Operators$ が文脈から明らかな場合，$\Ast[\Variables] = \Ast_\Operators[\Variables]$ のように $\Operators$ を省略する場合がある．
\end{definition}

なお，抽象構文木の規則において，IB2 と IS の規則は1つに統合することができる．よって，抽象構文木の規則を次のようにまとめる\footnote{I-Var の規則について $s \in \Sorts$ の束縛を，I-Op の規則について $\langle(s_1, \ldots, s_n), s\rangle \in \Arity(\Sorts)$ の束縛を，それぞれ文脈から明らかであると判断し省略している．今後もそのようにする．}\footnote{I-Op の規則のインスタンスは，$n = 0$ の場合 IB2 と同じ，つまり帰納法の基底となりうることに注意せよ．}．
\begin{description}
  \item[I-Var] $x \in \Variables_s$ について，$x \in \Ast_\Operators[\Variables]_s$．
  \item[I-Op] $o: (s_1, \ldots, s_n) \sortto_\Operators s$，$t_1 \in \Ast_\Operators[\Variables]_{s_1}, \ldots, t_n \in \Ast_\Operators[\Variables]_{s_n}$ について，$o(t_1; \cdots; t_n) \in \Ast_\Operators[\Variables]_s$．
\end{description}

\begin{principle}[抽象構文木に対する構造的帰納法]
  述語 $P$ について，以下が成り立つ時，任意の $s \in \Sorts$，$t \in \Ast_\Operators[\Variables]_s$ について $P(t)$ である．
  \begin{description}
    \item[I-Var] $x \in \Variables_s$ について，$P(x)$ が成り立つ．
    \item[I-Op] $o: (s_1, \ldots, s_n) \sortto_\Operators s$，$t_1 \in \Ast_\Operators[\Variables]_{s_1}, \ldots, t_n \in \Ast_\Operators[\Variables]_{s_n}$ について，$P(t_1), \ldots, P(t_n)$ が成り立つならば，$P(o(t_1; \cdots; t_n))$ が成り立つ．
  \end{description}
  この時，$t \in \Ast_\Operators[\Variables]_s$ について，$P(t)$ は $t$ に関する構造的帰納法で示されると言う．
\end{principle}

\begin{definition}[代入]
  $t \in \Ast_\Operators[\Variables, x \mapsto s']_s$，$t' \in \Ast_\Operators[\Variables]_{s'}$ について，$[t'/x]t \in \Ast_\Operators[\Variables]_s$ を，$t$ に関する構造的帰納法で定義する．
  \begin{description}
    \item[I-Var] $y \in \Variables_s$ について，
    \begin{align*}
      [t'/x]y = \left\{\begin{array}{ll}
        t' &(x = y) \\
        y &(x \neq y)
      \end{array}\right.\textend
    \end{align*}
    \item[I-Op] $o: (s_1, \ldots, s_n) \sortto_\Operators s$，$t_1 \in \Ast_\Operators[\Variables]_{s_1}, \ldots, t_n \in \Ast_\Operators[\Variables]_{s_n}$ について，
    \begin{align*}
      [t'/x]o(t_1; \cdots; t_n) = o([t'/x]t_1; \cdots; [t'/x]t_n)\textend
    \end{align*}
  \end{description}
\end{definition}

\begin{theorem}[代入の well-defined 性と決定性 (Theorem 1.1)]
  $t \in \Ast_\Operators[\Variables, x \mapsto s']_s$，$t' \in \Ast_\Operators[\Variables]_{s'}$ について，以下が成り立つ．
  \begin{align*}
    \exists ! \eta \in \Ast_\Operators[\Variables]_s\ldotp [t'/x]t = \eta
  \end{align*}
\end{theorem}
\begin{proof}
  以下のように，$t$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $y \in \Variables_s$ について，定義より，
    \begin{align*}
      [t'/x]y = \left\{\begin{array}{ll}
        t' &(x = y) \\
        y &(x \neq y)
      \end{array}\right.
    \end{align*}
    である．この場合一意に存在することは明らかである．よって，正しい．
    \item[I-Op] $o: (s_1, \ldots, s_n) \sortto_\Operators s$，$t_1 \in \Ast_\Operators[\Variables]_{s_1}, \ldots, t_n \in \Ast_\Operators[\Variables]_{s_n}$ について，定義より，
    \begin{align*}
      [t'/x]o(t_1; \cdots; t_n) = o([t'/x]t_1; \cdots; [t'/x]t_n)
    \end{align*}
    である．i.h. より，この時，$i \in [n]$ について $\zeta_i = [t'/x]t_i$ を満たす $\zeta_i$ が一意に存在する．ここから，
    \begin{align*}
      [t'/x]o(t_1; \cdots; t_n) = o(\zeta_1; \cdots; \zeta_n)
    \end{align*}
    が一意に存在する．よって，正しい．
  \end{description}
\end{proof}

\subsection{Abstract Binding Trees}

\begin{definition}[種価 (valence)]
  種の有限集合 $\Sorts$ について，$\langle (s_1, \ldots, s_n), s\rangle \in \Sorts^* \times \Sorts$を，種価と呼び，$(s_1, \ldots, s_n).s$ と表記する．種価の集合を $\Valence(\Sorts) = \Sorts^* \times \Sorts$ と表記する．
\end{definition}

\begin{definition}[一般化アリティ (generalized arity)]
  種の有限集合 $\Sorts$ について，$\langle (\upsilon_1, \ldots, \upsilon_n), s\rangle \in \Valence(\Sorts)^* \times \Sorts$ \footnote{本文では，$(\upsilon_1, \ldots, \upsilon_n)s$ と表記している．}を，一般化アリティと呼ぶ．一般化アリティの集合を $\GenArity(\Sorts) = \Valence(\Sorts)^* \times \Sorts$ と表記する．
\end{definition}

\begin{definition}[一般化演算子]
  種の有限集合 $\Sorts$ について，集合 $\Operators$ と関数 $\arity: \Operators \to \GenArity(\Sorts)$ の組 $\langle \Sorts, \arity\rangle$ を $\Sorts$-一般化演算子の集合\footnote{本文中では演算子と同様に扱われている．}と呼ぶ．$o \in \Operators$ をアリティ $\arity(o)$ を持つ一般化演算子と呼ぶ．また，文脈から $\arity$ が明らかな場合，単に $\Operators$ を演算子の集合と呼ぶ．アリティ $\langle (\upsilon_1, \ldots, \upsilon_n), s\rangle \in \GenArity(\Sorts)$ について，
  \begin{align*}
    (\upsilon_1, \ldots, \upsilon_n) \sortto_\Operators s = \{o \in \Operators \mid \arity(o) = \langle (\upsilon_1, \ldots, \upsilon_n), s\rangle\}
  \end{align*}
  と表記する．なお， $\Operators$ が文脈から明らかな場合，$(\upsilon_1, \ldots, \upsilon_n) \sortto s = (\upsilon_1, \ldots, \upsilon_n) \sortto_\Operators s$ のように $\Operators$ を省略する場合がある．
\end{definition}

\begin{definition}[フレッシュな再命名 (fresh renaming)]
  変数の集合 $\Variables$ において，$\vec{x}$ に対するフレッシュな再命名とは，全単射 $\rho: \vec{x} \bij \vec{x'}$ と $\Variables$ においてフレッシュな変数の列 $\vec{x'}$ の組 $\langle \rho, \vec{x'}\rangle$ のこと．なお，単に $\rho$ を再命名と呼ぶこともある．

  この時，$\hat{\rho} = a \mapsto a[x_i \assign x'_i]$ とおく．また，$\vec{x} \renaming_\Variables \vec{x'} = \{\rho \mid \text{フレッシュな再命名 $\rho: \vec{x} \bij \vec{x'}$}\}$ とおく．$\Variables$ が文脈から明らかな場合 $\vec{x} \renaming \vec{x'} = \vec{x} \renaming_\Variables \vec{x'}$ と $\Variables$ を省略する場合がある．
\end{definition}

\begin{definition}[抽象束縛木 (abstract binding tree, abt)]
  種の有限集合 $\Sorts$，$\Sorts$-一般化演算子の集合 $\Operators$，$\Sorts$-種付き変数の族 $\Variables$ について，$\Abt_\Operators[\Variables] = \indexedfamily{s \in \Sorts}{\Abt_\Operators[\Variables]_s}$ を以下のように帰納的に定義する．
  \begin{description}
    \item[IB1] $s \in \Sorts$，$x \in \Variables_s$ について，$x \in \Abt_\Operators[\Variables]_s$．
    \item[IB2] $s \in \Sorts$，$o: () \sortto_\Operators s$ について，$o \in \Abt_\Operators[\Variables]_s$．
    \item[IS] $\langle(\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n), s\rangle \in \GenArity(\Sorts)$，$n \geq 1$，$o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $\hat{\rho}(t_i) \in \Abt_\Operators[\Variables, \vec{x'_i}]_{s_i}$ となる $t_1, \ldots, t_n$ について，$o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n) \in \Abt_\Operators[\Variables]_s$．
  \end{description}
  この時，$t \in \Abt_\Operators[\Variables]$ を $\Variables$ で添字づけられた $\Operators$ による抽象束縛木と呼ぶ．なお，$\Operators$ が文脈から明らかな場合，$\Abt[\Variables] = \Abt_\Operators[\Variables]$ のように $\Operators$ を省略する場合がある．
\end{definition}

なお，抽象束縛木の規則において，IB2 と IS の規則は1つに統合することができる．よって，抽象束縛木の規則を次のようにまとめる\footnote{I-Var の規則について $s \in \Sorts$ の束縛を，I-Op の規則について $\langle(\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n), s\rangle \in \GenArity(\Sorts)$ の束縛を，それぞれ文脈から明らかであると判断し省略している．今後もそのようにする．}\footnote{I-Op の規則のインスタンスは，$n = 0$ の場合 IB2 と同じ，つまり帰納法の基底となりうることに注意せよ．}．
\begin{description}
  \item[I-Var] $x \in \Variables_s$ について，$x \in \Abt_\Operators[\Variables]_s$．
  \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $\hat{\rho}(t_i) \in \Abt_\Operators[\Variables, \vec{x'_i}]_{s_i}$ となる $t_1, \ldots, t_n$ について，$o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n) \in \Abt_\Operators[\Variables]_s$．
\end{description}

\begin{principle}[抽象束縛木に対する構造的帰納法]
  変数の集合で添字付けられた述語の族 $\indexedfamily{\Variables}{P[\Variables]}$ について，以下が成り立つ時，任意の $s \in \Sorts$，$t \in \Abt_\Operators[\Variables]_s$ について $P[\Variables](t)$ である．
  \begin{description}
    \item[I-Var] $x \in \Variables_s$ について，$P[\Variables](x)$ が成り立つ．
    \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $P[\Variables, \vec{x'_i}](\hat{\rho}(t_i))$ となる $t_1, \ldots, t_n$ について，$P[\Variables](o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n))$．
  \end{description}
  この時，$t \in \Abt_\Operators[\Variables]_s$ について，$P[\Variables](t)$ は $t$ に関する構造的帰納法で示されると言う．
\end{principle}

\begin{definition}[自由変数]
  $a \in \Abt[\Variables]$ について，$\FreeVars(a)$ を以下のように $a$ に関する構造的帰納法で定義する．
  \begin{description}
    \item[I-Var] $x \in \Variables$ について，$\FreeVars(x) = \{x\}$．
    \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，$t_1, \ldots, t_n$ について，$\FreeVars(o(\vec{x_1}.t_1, \ldots, \vec{x_n}.t_n)) = \bigcup_{i \in [n]} \FreeVars(t_i) \rcomplement \vec{x_i}$
  \end{description}
\end{definition}

\begin{lemma}
  \lemlabel{renaming-free-vars}
  $\rho: \vec{x} \renaming_\Variables \vec{x'}$，$\rho(a) \in \Abt[\Variables, \vec{x'}]$ について，$\FreeVars(a) \rcomplement \vec{x} = \FreeVars(\hat{\rho}(a)) \rcomplement \vec{x'}$．
\end{lemma}
\begin{proof}
  $a$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $y \in \Variables_s$ について，
    \begin{align*}
      \FreeVars(y) \backslash \vec{x}
      = \left\{\begin{array}{ll}
        \emptyset &(y \in \vec{x}) \\
        \{y\} &(\otherwise)
      \end{array}\right.
      = \left\{\begin{array}{ll}
        \emptyset &(\rho(y) \in \vec{x'}) \\
        \{\hat{\rho}(y)\} &(\otherwise)
      \end{array}\right.
      = \FreeVars(\hat{\rho}(y)) \backslash \vec{x'}
    \end{align*}
    より正しい．
    \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $\FreeVars(\hat{\rho_i}(t_i)) \backslash \vec{x} = \FreeVars(\hat{\rho}(\hat{\rho_i}(t_i))) \backslash \vec{x'}$ となる $t_1, \ldots, t_n$ について，
    \begin{align*}
      \FreeVars(o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n)) \backslash \vec{x}
      &= \left(\bigcup_{i \in [n]} \FreeVars(t_i) \rcomplement \vec{x_i}\right) \backslash \vec{x}m
    \end{align*}
    TODO
  \end{description}
\end{proof}

\begin{lemma}
  $a \in \Abt[\Variables]$ について，$\FreeVars(a) \subseteq \Variables$．
\end{lemma}
\begin{proof}
  $a$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $x \in \Variables_s$ について，$\FreeVars(x) = \{x\} \subseteq \Variables$ より正しい．
    \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $\FreeVars(\hat{\rho}(t_i)) \subseteq \Variables, \vec{x'_i}$ となる $t_1, \ldots, t_n$ について，
    \begin{align*}
      \FreeVars(o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n))
      &= \bigcup_{i \in [n]} \FreeVars(t_i) \rcomplement \vec{x_i}
    \end{align*}
    TODO
  \end{description}
\end{proof}

\begin{definition}[$\alpha$-同値性]
  $a_1, a_2 \in \Abt[\Variables]_s$ について，$a_1 =_\alpha a_2$ を以下のように構造的帰納法で定義する．
  \begin{description}
    \item[I-Var] $x \in \Variables_s$ について，$x =_\alpha x$
    \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_{1,i}: \vec{x_{1,i}} \renaming \vec{x'_i}$，$\rho_{2,i}: \vec{x_{2,i}} \renaming \vec{x'_i}$ について $\rho_{1,i}(t_{1,i}) =_\alpha \rho_{2,i}(t_{2,i})$ となる $t_{1,1}, \ldots, t_{1,n}, t_{2,1}, \ldots, t_{2,n}$ について，$o(\vec{x_{1,1}}.t_{1,1}; \cdots; \vec{x_{1,n}}.t_{1,n}) =_\alpha o(\vec{x_{2,1}}.t_{2,1}; \cdots; \vec{x_{2,n}}.t_{2,n})$．
  \end{description}
\end{definition}

\begin{lemma}
  \lemlabel{alpha-equiv-not-be-considered-free-vars}
  $a_1, a_2 \in \Abt[\Variables]_s$ について，$a_1 =_\alpha a_2$ の時，$\FreeVars(a_1) = \FreeVars(a_2)$．
\end{lemma}
\begin{proof}
  $a_1 =_\alpha a_2$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $x =_\alpha x$ の時，明らか．
    \item[I-Op] $o(\vec{x_{1,1}}.t_{1,1}; \cdots; \vec{x_{1,n}}.t_{1,n}) =_\alpha o(\vec{x_{2,1}}.t_{2,1}; \cdots; \vec{x_{2,n}}.t_{2,n})$ の時，任意の $i \in [n]$，$\rho_{1,i}: \vec{x_{1,i}} \renaming \vec{x'_i}$，$\rho_{2,i}: \vec{x_{2,i}} \renaming \vec{x'_i}$ について，inversion lemma より $\rho_1(t_{1,i}) =_\alpha \rho_2(t_{2,i})$ より i.h. から，$\FreeVars(\rho_1(t_{1,i})) = \FreeVars(\rho_2(t_{2,i}))$ である．ここから，
    \begin{align*}
      \FreeVars(t_{1,i}) \rcomplement \vec{x_{1,i}}
      &= \FreeVars(\rho_1(t_{1,i})) \rcomplement \vec{x'_i} &(\because \text{\lemref{renaming-free-vars}}) \\
      &= \FreeVars(\rho_2(t_{2,i})) \rcomplement \vec{x'_i} &(\because \FreeVars(\rho_1(t_{1,i})) = \FreeVars(\rho_2(t_{2,i}))) \\
      &= \FreeVars(t_{2,i}) \rcomplement \vec{x_{2,i}} &(\because \text{\lemref{renaming-free-vars}})
    \end{align*}
    であり，
    \begin{align*}
      \FreeVars(o(\vec{x_{1,1}}.t_{1,1}; \cdots; \vec{x_{1,n}}.t_{1,n}))
      &= \bigcup_{i \in [n]} \FreeVars(t_{1,i}) \rcomplement \vec{x_{1,i}} \\
      &= \bigcup_{i \in [n]} \FreeVars(t_{2,i}) \rcomplement \vec{x_{2,i}} \\
      &= \FreeVars(o(\vec{x_{2,1}}.t_{2,1}; \cdots; \vec{x_{2,n}}.t_{2,n}))
    \end{align*}
    より正しい．
  \end{description}
\end{proof}

\begin{definition}[代入]
  $a \in \Abt[\Variables]$，$x \in \Variables_s$，$b \in \Abt[\Variables]_s$ について，$[b/x]a$ を以下のように $a$ に関する構造的帰納法で定義する．
  \begin{description}
    \item[I-Var] $y \in \Variables_s$ について，
    \begin{align*}
      [b/x]y = \left\{\begin{array}{ll}
        b &(x = y) \\
        y &(\otherwise)
      \end{array}\right.
    \end{align*}
    \item[I-Op] $o: (\vec{s_1}.s_1, \ldots, \vec{s_n}.s_n) \sortto_\Operators s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $\hat{\rho}(t_i) \in \Abt_\Operators[\Variables, \vec{x'_i}]_{s_i}$ となる $t_1, \ldots, t_n$ について，$t'_1, \ldots, t'_n$ を以下のようにおく．
    \begin{align*}
      t'_i = \left\{\begin{array}{ll}
        t_i &(x \in \vec{x_i}) \\
        {[}b/x]t_i &(\otherwise)
      \end{array}\right.
    \end{align*}
    この時，$[b/x]o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n) = o(\vec{x_1}.t'_1; \cdots; \vec{x_n}.t'_n)$
  \end{description}
\end{definition}

\begin{lemma}
  \lemlabel{substitute-free-vars}
  $x \not\in \FreeVars(a)$ の時，$[b/x]a = a$．
\end{lemma}
\begin{proof}
  TODO
\end{proof}

\begin{exercise}[1.1]
  \begin{align*}
    \mathcal{X} \subseteq \mathcal{Y} \implies \Ast[\mathcal{X}] \subseteq \Ast[\mathcal{Y}]
  \end{align*}
  を構造的帰納法で示せ．
\end{exercise}
\begin{proof}[解答]
  $\mathcal{X} \subseteq \mathcal{Y}$ の時，
  \begin{align*}
    P(t) \textiff t \in \Ast[\mathcal{Y}]
  \end{align*}
  を $t \in \Ast[\mathcal{X}]$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $x \in \mathcal{X}_s$ について，$x \in \mathcal{Y}_s$．よって，I-Var の規則から $x \in \Ast[\mathcal{Y}]$ より正しい．
    \item[I-Op] $o: (s_1, \ldots, s_n) \sortto s$，$t_1, \ldots, t_n \in \Ast[\mathcal{X}]$ について，i.h. より $t_1, \ldots, t_n \in \Ast[\mathcal{Y}]$．よって，I-Op の規則から $o(t_1; \cdots; t_n) \in \Ast[\mathcal{Y}]$ より正しい．
  \end{description}
  $\forall t \in \Ast[\mathcal{X}]. P(t) \iff \Ast[\mathcal{X}] \subseteq \Ast[\mathcal{Y}]$ より題意は示された．
\end{proof}

\begin{exercise}[1.2]
  \begin{align*}
    \mathcal{X} \subseteq \mathcal{Y} \implies \Abt[\mathcal{X}] \subseteq \Abt[\mathcal{Y}]
  \end{align*}
  を構造的帰納法で示せ．
\end{exercise}
\begin{proof}[解答]
  $\mathcal{Y}$ について，
  \begin{align*}
    P[\mathcal{X}](t) \textiff \mathcal{X} \subseteq \mathcal{Y} \implies t \in \Abt[\mathcal{Y}]
  \end{align*}
  を $t \in \Abt[\mathcal{X}]$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $x \in \mathcal{X}_s$ について，$\mathcal{X} \subseteq \mathcal{Y}$ とする．この時，$x \in \mathcal{Y}_s$．よって，I-Var の規則から $x \in \Abt[\mathcal{Y}]$ より正しい．
    \item[I-Op] $o: (s_1, \ldots, s_n) \sortto s$，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $P[\mathcal{X}, \vec{x'_i}](\hat{\rho}(t_i))$ となる $t_1, \ldots, t_n$ について，$\mathcal{X} \subseteq \mathcal{Y}$ とする．この時，i.h. より，任意の $i \in [n]$，$\rho_i: \vec{x_i} \renaming \vec{x'_i}$ について $\hat{\rho}(t_i) \in \Abt[\mathcal{Y}, \vec{x'_i}]$．よって，I-Op の規則から $o(\vec{x_1}.t_1; \cdots; \vec{x_n}.t_n) \in \Abt[\mathcal{Y}]$ より正しい．
  \end{description}
  $\forall t \in \Abt[\mathcal{X}]. P[\mathcal{X}](t) \textiff \mathcal{X} \subseteq \mathcal{Y} \implies \Abt[\mathcal{X}] \subseteq \Abt[\mathcal{Y}]$ より題意は示された．
\end{proof}

\begin{exercise}[1.3]
  $a_1 =_\alpha a_2$，$b_1 =_\alpha b_2$ かつ $[b_1/x]a_1$，$[b_2/x]a_2$ が定義される時，$[b_1/x]a_1 =_\alpha [b_2/x]a_2$ を示せ．
\end{exercise}
\begin{proof}[解答]
  $a_1 =_\alpha a_2$ に関する構造的帰納法で示す．
  \begin{description}
    \item[I-Var] $y =_\alpha y$ の時，
    \begin{align*}
      [b_1/x]y = \left\{\begin{array}{ll}
        b_1 &(y = x) \\
        y &(\otherwise)
      \end{array}\right. =_\alpha \left\{\begin{array}{ll}
        b_2 &(y = x) \\
        y &(\otherwise)
      \end{array}\right. = [b_2/x]y
    \end{align*}
    より正しい．
    \item[I-Op] $o(\vec{x_{1,1}}.t_{1,1}; \cdots; \vec{x_{1,n}}.t_{1,n}) =_\alpha o(\vec{x_{2,1}}.t_{2,1}; \cdots; \vec{x_{2,n}}.t_{2,n})$ の時，任意の $i \in [2]$，$j \in [n]$ について，
    \begin{align*}
      t'_{i,j} = \left\{\begin{array}{ll}
        t_{i,j} &(x \in \vec{x_{i,j}}) \\
        {[}b_i/x]t_{i,j} &(\otherwise)
      \end{array}\right.
    \end{align*}
    とおく．この時，任意の $i \in [n]$，$\rho_1: \vec{x_{1,i}} \renaming \vec{x'_i}$，$\rho_2: \vec{x_{2,i}} \renaming \vec{x'_i}$ について，以下の場合分けで $\hat{\rho_1}(t'_{1,i}) =_\alpha \hat{\rho_2}(t'_{2,i})$ を示す．
    \begin{itemize}
      \item $x \in \vec{x_{1,i}}$ かつ $x \in \vec{x_{2,i}}$ の時，
      \begin{align*}
        \hat{\rho_1}(t'_{1,i})
        &= \hat{\rho_1}(t_{1,i}) \\
        &=_\alpha \hat{\rho_2}(t_{2,i}) &(\because \text{inversion lemma}) \\
        &= \hat{\rho_2}(t'_{2,i})
      \end{align*}
      より正しい．
      \item $x \not\in \vec{x_{1,i}}$ かつ $x \not\in \vec{x_{2,i}}$ の時，
      \begin{align*}
        \hat{\rho_1}(t'_{1,i})
        &= \hat{\rho_1}([b_1/x]t_{1,i}) \\
        &=_\alpha \hat{\rho_2}([b_2/x]t_{2,i}) &(\because \text{i.h.}) \\
        &= \hat{\rho_2}(t'_{2,i})
      \end{align*}
      より正しい．
      \item それ以外の時対称性より $x \in \vec{x_{1,i}}$ かつ $x \not\in \vec{x_{2,i}}$ の時だけを考える．$x \in \FreeVars(t_{2,i})$ の時，$x \in \FreeVars(\hat{\rho_2}(t_{2,i}))$ である．inversion lemma より $\hat{\rho_1}(t_{1,i}) =_\alpha \hat{\rho_2}(t_{2,i})$ であり，\lemref{alpha-equiv-not-be-considered-free-vars}から $\FreeVars(\hat{\rho_1}(t_{1,i})) = \FreeVars(\hat{\rho_2}(t_{2,i}))$ であるため，$x \in \FreeVars(\hat{\rho_1}(t_{1,i}))$ である．この時，\lemref{renaming-free-vars}より $x \in \vec{x'_i}$ であり，$\rho_2$ はフレッシュな再命名より $x \not\in \FreeVars(t_{2,i})$ となるが，これは矛盾．よって，$x \not\in \FreeVars(t_{2,i})$ であり，この時，
      \begin{align*}
        \hat{\rho_1}(t'_{1,i})
        &= \hat{\rho_1}(t_{1,i}) \\
        &=_\alpha \hat{\rho_2}(t_{2,i}) \\
        &= \hat{\rho_2}([b_2/x]t_{2,i}) &(\because \text{\lemref{substitute-free-vars}}) \\
        &= \hat{\rho_2}(t'_{2,i})
      \end{align*}
      より正しい．
    \end{itemize}
    ゆえに
    \begin{align*}
      [b_1/x]o(\vec{x_{1,1}}.t_{1,1}; \cdots; \vec{x_{1,n}}.t_{1,n})
      &= o(\vec{x_{1,1}}.t'_{1,1}; \cdots; \vec{x_{1,n}}.t'_{1,n}) \\
      &=_\alpha o(\vec{x_{2,1}}.t'_{2,1}; \cdots; \vec{x_{2,1}}.t'_{2,n}) \\
      &= [b_2/x]o(\vec{x_{2,1}}.t_{2,1}; \cdots; \vec{x_{2,n}}.t_{2,n})
    \end{align*}
    より正しい．
  \end{description}
\end{proof}
