# LaTeX Slide Template

## Create new slide

```bash
newslide <DIR>
```

See also:

```bash
newslide --help
```

## Installation

```bash
make
```

or using `docker-compose`

### Dependencies

* Pipenv
* Tlmgr
* pLaTeX
* LatexMk
* findutils (for clean task)
* git supports (optional: See https://git-scm.com/)
* editorconfig supports (optional: See http://editorconfig.org)

## Watching and Auto Building

```
make watch
```

## How to write slides?

1. Make article file in `article/`
1. Write beamer frames
1. run `make` and preview `main.pdf`

Also see [article/sample.tex](article/sample.tex) and `*.tex` files.

## How to customize?

TODO
